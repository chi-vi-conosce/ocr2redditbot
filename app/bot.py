from telegram.ext import Updater, MessageHandler, Filters
import json
import re
import random
import requests
from subprocess import check_output
from tempfile import NamedTemporaryFile

with open('/app/config.json', 'r') as config_file:
    config = json.load(config_file)

token = config['token']
bot_name = config['bot_name']
trigger_words = config['trigger_words']
subreddit = config['subreddit']

if trigger_words is None or len(trigger_words) == 0 or subreddit is None:
    print('Invalid config.json')
    exit(1)


def get_image_link():
    url = f'https://www.reddit.com/r/{subreddit}/hot.json'
    headers = {'User-agent': bot_name}
    posts = requests.get(url, headers=headers).json()['data']['children']
    index = random.randint(0, len(posts) - 1)
    return posts[index]['data']['url']

def process(update, image):
    user = update.message.from_user
    print(f'Request from {user}')
    temp_file = NamedTemporaryFile()
    img_file = temp_file.name
    image.download(img_file)
    cmd = ['/usr/bin/tesseract', '--dpi', '300', img_file, 'stdout']
    output = check_output(cmd).decode().strip().lower()
    if output:
        for word in trigger_words:
            if re.search(word, output):
                image_link = get_image_link()
                update.message.reply_text(image_link)


def image_handler(update, context):
    photo_handle = update.message.photo[-1].get_file()
    process(update, photo_handle)


def sticker_handler(update, context):
    if update.message.sticker.is_animated:
        return
    sticker_id = update.message.sticker.file_id
    sticker_handle = context.bot.get_file(sticker_id, timeout=None)
    process(update, sticker_handle)


updater = Updater(token)
updater.dispatcher.add_handler(MessageHandler(Filters.photo, image_handler))
updater.dispatcher.add_handler(MessageHandler(Filters.sticker, sticker_handler))
updater.start_polling()
updater.idle()
