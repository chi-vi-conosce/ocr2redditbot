FROM alpine:3.13

RUN apk --no-cache add py3-pip~=20 tesseract-ocr~=4 tesseract-ocr-data-ita~=4 \
&& pip install --no-cache-dir python-telegram-bot==13.* requests==2.* \
&& adduser --no-create-home --disabled-password --shell /sbin/nologin appuser

USER appuser
